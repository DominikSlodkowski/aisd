from typing import Any


class Node:
    value: Any
    next: 'Node'

    def __init__(self, value: Any, next_: 'Node') -> None:
        self.value = value
        self.next = next_


class LinkedList:
    head: Node
    tail: Node

    def __init__(self) -> None:
        self.head = None
        self.tail = None

    def __str__(self) -> str:
        result: str = str(self.head.value)
        node_: Node = self.head
        while node_.next is not None:
            node_ = node_.next
            result += (' -> ' + str(node_.value))
        return result

    def push(self, value: Any) -> None:
        if self.head is None:
            node_ = Node(value, None)
            self.head = node_
            self.tail = node_
        else:
            node_: Node = self.head
            self.head = Node(value, node_)

    def append(self, value: Any) -> None:
        if self.head is None:
            node_ = Node(value, None)
            self.head = node_
            self.tail = node_
        else:
            node_ = Node(value, None)
            self.tail.next = node_
            self.tail = node_

    def node(self, at: int) -> Node:
        node_: Node = self.head
        for i in range(0, at):
            node_ = node_.next
        return node_

    def insert(self, value: Any, after: Node) -> None:
        if after.next is None:
            node_: Node = Node(value, after.next)
            after.next = node_
            self.tail = node_
        else:
            node_: Node = Node(value, after.next)
            after.next = node_

    def pop(self) -> Any:
        value: Any = self.head.value
        self.head = self.head.next
        return value

    def remove_last(self) -> Any:
        value = self.tail.value
        self.tail = self.node(len(self)-2)
        self.tail.next = None
        return value

    def __len__(self) -> int:
        node_ = self.head
        len_ = 0
        if node_.value is None:
            return len_
        len_ += 1
        while node_.next is not None:
            node_ = node_.next
            len_ += 1
        return len_

    def remove(self, after: Node) -> None:
        after.next = after.next.next


class Stack:
    _storage: LinkedList

    def __init__(self) -> None:
        self._storage = LinkedList()

    def push(self, element: Any) -> None:
        self._storage.append(element)

    def pop(self) -> Any:
        result: Any = self._storage.remove_last()
        return result

    def __len__(self) -> int:
        if self._storage.head is None:
            return 0
        return len(self._storage)

    def __str__(self) -> str:
        return str(self._storage)


class Queue:
    _storage: LinkedList

    def __init__(self) -> None:
        self._storage = LinkedList()

    def peek(self) -> Any:
        return self._storage.head.value

    def enqueue(self, element: Any) -> None:
        self._storage.append(element)

    def dequeue(self) -> Any:
        return self._storage.pop()

    def __str__(self) -> str:
        result: str = str(self._storage.head.value)
        node_: Node = self._storage.head
        while node_.next is not None:
            node_ = node_.next
            result += (', ' + str(node_.value))
        return result

    def __len__(self) -> int:
        if self._storage.head is None:
            return 0
        return len(self._storage)


# Test Listy
list_ = LinkedList()
assert list_.head is None

list_.push(1)
list_.push(0)
assert str(list_) == '0 -> 1'

list_.append(9)
list_.append(10)
assert str(list_) == '0 -> 1 -> 9 -> 10'

print(list_)

middle_node = list_.node(at=1)
list_.insert(5, after=middle_node)

print(list_)
assert str(list_) == '0 -> 1 -> 5 -> 9 -> 10'

first_element = list_.node(at=0)
returned_first_element = list_.pop()

assert first_element.value == returned_first_element
print(list_)

last_element = list_.node(at=3)
returned_last_element = list_.remove_last()

assert last_element.value == returned_last_element
assert str(list_) == '1 -> 5 -> 9'
print(list_)

second_node = list_.node(at=1)
list_.remove(second_node)

assert str(list_) == '1 -> 5'
print(list_)

# Test Stosu
stack = Stack()

assert len(stack) == 0

stack.push(3)
stack.push(10)
stack.push(1)

assert len(stack) == 3

print(stack)

top_value = stack.pop()

assert top_value == 1

assert len(stack) == 2

# Kolejka
queue = Queue()

assert len(queue) == 0

queue.enqueue('klient1')
queue.enqueue('klient2')
queue.enqueue('klient3')

print(queue)
assert str(queue) == 'klient1, klient2, klient3'

client_first = queue.dequeue()

assert client_first == 'klient1'
assert str(queue) == 'klient2, klient3'
assert len(queue) == 2
print(queue)

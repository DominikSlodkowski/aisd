from typing import Callable, List, Tuple, Dict


def zad_1(name_letter: str, surname: str) -> str:
    return name_letter + '.' + surname


def zad_2(name: str, surname: str) -> str:
    return name[0].upper() + '.' + surname[0].upper() + surname[1:].lower()


def zad_3(year_1: int, year_2: int, age: int) -> int:
    return year_1 * 100 + year_2 - age


def zad_4(name: str, surname: str, fun: Callable[[str, str], str]) -> str:
    return fun(name, surname)


def zad_5(var_1: float, var_2: float) -> float:
    if var_1 > 0 and var_2 > 0 and var_2 != 0:
        return var_1 / var_2
    return 0


def zad_6() -> None:
    suma: float = 0
    liczba: float
    while suma < 100:
        suma += int(input('Podaj liczbe: '))


def zad_7(li: List[any]) -> Tuple[any]:
    return tuple(li)


def zad_9(liczba: int) -> str:
    dni: Dict[int: str] = {
        1: 'Poniedzialek',
        2: 'Wtorek',
        3: 'Sroda',
        4: 'Czwartek',
        5: 'Piatek',
        6: 'Sobota',
        7: 'Niedziela'
    }
    return dni[liczba]


def zad_10(text: str) -> bool:
    for i in (0, len(text)//2):
        if text[i] != text[len(text) - i - 1]:
            return False
    return True


print(zad_4('Dominik', 'Słodkowski', zad_2))

from typing import Any, Callable, Union, List, Optional


class BinaryNode:
    value: Any
    left_child: Union['BinaryNode', None] = None
    right_child: Union['BinaryNode', None] = None

    def __init__(self, value: Any) -> None:
        self.value = value

    def is_leaf(self) -> bool:
        if self.left_child is None and self.right_child is None:
            return True
        return False

    def add_left_child(self, value: Any) -> None:
        self.left_child = BinaryNode(value)

    def add_right_child(self, value: Any) -> None:
        self.right_child = BinaryNode(value)

    def sum(self) -> int:
        result: int = self.value
        if self.left_child is not None:
            result += self.left_child.sum()
        if self.right_child is not None:
            result += self.right_child.sum()
        return result

    def traverse_in_order(self, visit: Callable[[Any], None]) -> None:
        if self.left_child is not None:
            self.left_child.traverse_in_order(visit)
        visit(self.value)
        if self.right_child is not None:
            self.right_child.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]) -> None:
        if self.left_child is not None:
            self.left_child.traverse_post_order(visit)
        if self.right_child is not None:
            self.right_child.traverse_post_order(visit)
        visit(self)

    def traverse_pre_order(self, visit: Callable[[Any], None]) -> None:
        visit(self)
        if self.left_child is not None:
            self.left_child.traverse_pre_order(visit)
        if self.right_child is not None:
            self.right_child.traverse_pre_order(visit)

    def children(self) -> List[Optional['BinaryNode']]:
        children_list: List[Union[BinaryNode, None]] = []
        if self.left_child:
            children_list.append(self.left_child)
        if self.right_child:
            children_list.append(self.right_child)
        return children_list

    def __str__(self) -> str:
        return str(self.value)


class BinaryTree:
    root: BinaryNode

    def __init__(self, root_value: Any):
        self.root = BinaryNode(root_value)

    def traverse_in_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_in_order(visit)

    def traverse_post_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_post_order(visit)

    def traverse_pre_order(self, visit: Callable[[Any], None]) -> None:
        self.root.traverse_pre_order(visit)

    def show(self, node: 'BinaryNode' = None, level: int = 0) -> None:
        if node is None and level == 0:
            node = self.root
        if node is not None:
            self.show(node.left_child, level + 1)
            print('    ' * level + '->', node.value)
            self.show(node.right_child, level + 1)


class Node:
    value: Any
    next: Union['Node', None]

    def __init__(self, value: Any, next_: Union['Node', None]) -> None:
        self.value = value
        self.next = next_


class LinkedList:
    head: Union[Node, None]
    tail: Union[Node, None]

    def __init__(self) -> None:
        self.head = None
        self.tail = None

    def __str__(self) -> str:
        result: str = str(self.head.value)
        node_: Node = self.head
        while node_.next is not None:
            node_ = node_.next
            result += (' -> ' + str(node_.value))
        return result

    def push(self, value: Any) -> None:
        if self.head is None:
            node_ = Node(value, None)
            self.head = node_
            self.tail = node_
        else:
            node_: Node = self.head
            self.head = Node(value, node_)

    def append(self, value: Any) -> None:
        if self.head is None:
            node_ = Node(value, None)
            self.head = node_
            self.tail = node_
        else:
            node_ = Node(value, None)
            self.tail.next = node_
            self.tail = node_

    def node(self, at: int) -> Node:
        node_: Node = self.head
        for i in range(0, at):
            node_ = node_.next
        return node_

    def insert(self, value: Any, after: Node) -> None:
        if after.next is None:
            node_: Node = Node(value, after.next)
            after.next = node_
            self.tail = node_
        else:
            node_: Node = Node(value, after.next)
            after.next = node_

    def pop(self) -> Any:
        value: Any = self.head.value
        self.head = self.head.next
        return value

    def remove_last(self) -> Any:
        value = self.tail.value
        self.tail = self.node(len(self)-2)
        self.tail.next = None
        return value

    def __len__(self) -> int:
        node_ = self.head
        len_ = 0
        if node_.value is None:
            return len_
        len_ += 1
        while node_.next is not None:
            node_ = node_.next
            len_ += 1
        return len_

    def remove(self, after: Node) -> None:
        after.next = after.next.next


class Queue:
    _storage: LinkedList

    def __init__(self) -> None:
        self._storage = LinkedList()

    def peek(self) -> Any:
        return self._storage.head.value

    def enqueue(self, element: Any) -> None:
        self._storage.append(element)

    def dequeue(self) -> Any:
        return self._storage.pop()

    def __str__(self) -> str:
        result: str = str(self._storage.head.value)
        node_: Node = self._storage.head
        while node_.next is not None:
            node_ = node_.next
            result += (', ' + str(node_.value))
        return result

    def __len__(self) -> int:
        if self._storage.head is None:
            return 0
        return len(self._storage)


# Użyty do testu przechodzenia po drzewie
def printujacy_visit(value: Any) -> None:
    print(f"wartosc node: {value} ")


def horizontal_sum(tree: 'BinaryTree') -> List[int]:
    result: List[int] = []
    queue: Queue = Queue()
    queue.enqueue(tree.root)
    while len(queue) > 0:
        level_size = len(queue)
        level_sum = 0
        while level_size > 0:
            current_node: BinaryNode = queue.dequeue()
            level_sum += current_node.value
            for child in current_node.children():
                queue.enqueue(child)
            level_size -= 1
        result.append(level_sum)
    return result


# Pierwsze drzewo
tree: BinaryTree = BinaryTree(10)
tree.root.add_left_child(9)
tree.root.left_child.add_left_child(1)
tree.root.left_child.add_right_child(3)
tree.root.add_right_child(2)
tree.root.right_child.add_left_child(4)
tree.root.right_child.add_right_child(6)

assert tree.root.value == 10

assert tree.root.right_child.value == 2
assert tree.root.right_child.is_leaf() is False

assert tree.root.left_child.left_child.value == 1
assert tree.root.left_child.left_child.is_leaf() is True

tree.traverse_pre_order(printujacy_visit)
tree.show()
li1: List[int] = horizontal_sum(tree)
for el in li1:
    print(el)

# Drugie drzewo
print("\n\n")
t: BinaryTree = BinaryTree(1)
t.root.add_left_child(2)
t.root.left_child.add_left_child(3)
t.root.left_child.add_right_child(4)
t.root.left_child.right_child.add_left_child(5)
t.root.left_child.right_child.add_right_child(6)
t.root.left_child.right_child.left_child.add_right_child(7)

t.show()
t.traverse_in_order(printujacy_visit)
li2: List[int] = horizontal_sum(t)
for el in li2:
    print(el)
